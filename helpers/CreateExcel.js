var xl = require("excel4node");

var Excel = function () {

};

Excel.prototype.createFile = function (excelFileName, callback) {
    var wb = new xl.Workbook();
    var reportHeadeStyle = wb.createStyle({
        font: {
            size: 16,
            bold: true
        }
    });
    var headStyle = wb.createStyle({
        font: {
            size: 12,
            bold: true
        },
        alignment: {
            horizontal: 'center',
            vertical: 'center'
        },
        border: {
            left: { style: 'thin' },
            right: { style: 'thin' },
            top: { style: 'thin' },
            bottom: { style: 'thin' }
        },
        fill: {
            type: 'pattern',
            patternType: 'solid',
            fgColor: '#D9E1F2'
        }
    });
    var bodyStyle = wb.createStyle({
        font: {
            size: 12,
        },
        border: {
            left: { style: 'thin' },
            right: { style: 'thin' },
            top: { style: 'thin' },
            bottom: { style: 'thin' }
        },
        numberFormat: '#,##0.00; (#,##0.00); -'
    });
    var ws = wb.addWorksheet('Sheet 1');
    ws.cell(1, 1).string("Report xxx").style(reportHeadeStyle);

    ws.cell(2, 1).string('H1').style(headStyle);
    ws.cell(2, 2).string('H2').style(headStyle);
    ws.cell(2, 3).string('H3').style(headStyle);
    ws.cell(2, 4).string('H4').style(headStyle);
    ws.cell(2, 5).string('H5');

    ws.column(1).setWidth(10);
    ws.column(2).setWidth(35);
    ws.column(3).setWidth(18);
    ws.column(4).setWidth(27);
    ws.column(5).setWidth(32);

    var raw = [
        {
            "rown": 1,
            "c": "a",
            "n": 5000,
            "d": "01/01/2017",
            "x": "xxxx"
        },
        {
            "rown": 2,
            "c": "b",
            "n": 70,
            "d": "02/01/2017",
            "x": "xxxx"
        },
        {
            "rown": 3,
            "c": "c",
            "n": 600,
            "d": "09/01/2017",
            "x": "xxxx"
        },
        {
            "rown": 4,
            "c": "a",
            "n": 50,
            "d": "01/01/2017",
            "x": "xxxx"
        }
    ];
    raw.forEach(function (value, index, arr) {
        ws.cell(index + 3, 1).number(value.rown).style({
            font: {
                size: 12
            },
            border: {
                left: { style: 'thin' },
                right: { style: 'thin' },
                top: { style: 'thin' },
                bottom: { style: 'thin' }
            },
            alignment: {
                horizontal: 'center',
                vertical: 'center'
            },
        });
        ws.cell(index + 3, 2).string(value.c).style(bodyStyle);
        ws.cell(index + 3, 3).number(value.n).style(bodyStyle);
        ws.cell(index + 3, 4).string(value.d).style(bodyStyle);
        ws.cell(index + 3, 5).string(value.x).style(bodyStyle);
    });
    wb.write(excelFileName, callback);
}

module.exports = new Excel();